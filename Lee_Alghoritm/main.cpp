#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>

const int WALL = -1;         // ������������ ������
const int BLANK = -2;         // ��������� ������������ ������

void show_grid(int **field, int n);
void show_workfield(int **field, int n);
bool lee(int ax, int ay, int bx, int by, int ** grid, int*px, int*py, int W, int H);   // ����� ���� �� ������ (ax, ay) � ������ (bx, by)

int main(void)
{
	int n, *px, *py, x, y, **grid; 
	char checked_quit[5] = "quit", **show_field;

	printf("Input n: ");
	scanf("%d", &n);
	px = (int*)malloc(n*n * sizeof(int));
	py = (int*)malloc(n*n * sizeof(int));
	grid = (int**)malloc(n * sizeof(int));
	for (size_t i = 0; i < n; i++)
		grid[i] = (int*)malloc(n * sizeof(int));
	int number_of_balls;
	printf("Input how many balls do you want: "); scanf("%d", &number_of_balls);
	printf("Input coordinates of balls (x, y)\n");
	for (int i = 0; i < number_of_balls; i++) {
		scanf("%d%d", &x, &y);
		grid[x][y] = WALL;
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (grid[i][j] != WALL)
				grid[i][j] = BLANK;
		}
	}
	show_workfield(grid, n);
	printf("Input coordinates of ball, which you want to move: \n");
	int ax, ay, bx, by;
	scanf("%d%d", &ax, &ay);
	if (grid[ax][ay] != -1) 
		printf("It`s not a ball.\n");
	else
	{
		printf("Input coordinates of place, which you want to move to:\n");
		scanf("%d%d", &bx, &by);
		if (grid[bx][by] == -1)
			printf("You can`t move the ball to this place.\n");
		else
		{

			lee(ay, ax, by, bx, grid, px, py, n, n);
		}
	}
	
	int stop; scanf("%d", &stop);
	return 0;
}

void show_workfield(int **field, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (field[i][j] == -2) printf(" - ");
			if (field[i][j] == -1) printf(" 0 ");
		}
		printf("\n");
	}
}

void show_grid(int **field, int n) {
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			printf("%-3d", field[i][j]);
		}
		printf("\n");
	}
}

bool lee(int ax, int ay, int bx, int by, int ** grid, int*px, int*py, int W, int H)   // ����� ���� �� ������ (ax, ay) � ������ (bx, by)
{
	int len;
	int dx[4] = { 1, 0, -1, 0 };   // ��������, ��������������� ������� ������
	int dy[4] = { 0, 1, 0, -1 };   // ������, �����, ����� � ������
	int d, x, y, k;
	bool stop;

	if (grid[by][bx] == WALL) return false;  // ������ (ax, ay) ��� (bx, by) - �����

											 // ��������������� �����
	d = 0;
	grid[ay][ax] = 0;            // ��������� ������ �������� 0
	do {
		stop = true;               // ������������, ��� ��� ��������� ������ ��� ��������
		for (y = 0; y < H; ++y)
			for (x = 0; x < W; ++x)
				if (grid[y][x] == d)                         // ������ (x, y) �������� ������ d
				{
					for (k = 0; k < 4; ++k)                    // �������� �� ���� ������������ �������
					{
						int iy = y + dy[k], ix = x + dx[k];
						if (iy >= 0 && iy < H && ix >= 0 && ix < W &&
							grid[iy][ix] == BLANK)
						{
							stop = false;              // ������� ������������ ������
							grid[iy][ix] = d + 1;      // �������������� �����
						}
					}
				}
		d++;
	} while (!stop && grid[by][bx] == BLANK);

	if (grid[by][bx] == BLANK) return false;  // ���� �� ������

											  // �������������� ����
	len = grid[by][bx];            // ����� ����������� ���� �� (ax, ay) � (bx, by)
	x = bx;
	y = by;
	d = len;
	while (d > 0)
	{
		px[d] = x;
		py[d] = y;                   // ���������� ������ (x, y) � ����
		d--;
		for (k = 0; k < 4; ++k)
		{
			int iy = y + dy[k], ix = x + dx[k];
			if (iy >= 0 && iy < H && ix >= 0 && ix < W &&
				grid[iy][ix] == d)
			{
				x = x + dx[k];
				y = y + dy[k];           // ��������� � ������, ������� �� 1 ����� � ������
				break;
			}
		}
	}
	px[0] = ax;
	py[0] = ay;  // ������ px[0..len] � py[0..len] - ���������� ����� ����
	bool s;
	for (size_t i = 0; i < W; i++)
	{
		for (size_t j = 0; j < W; j++)
		{
			s = 0;
			for (int z = 0; z <= len; z++)
			{
				if (py[z] == i && px[z] == j)
				{
					printf(" . "); s = 1;
				}
			}
			if (s == 0) printf(" - ");
		}
		printf("\n");
	}
	return true;
}
